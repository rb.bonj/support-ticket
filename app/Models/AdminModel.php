<?php namespace App\Models;

use CodeIgniter\Model;

class AdminModel extends Model{

    
    protected $table = 'support_staff';
    protected $primaryKey = 'id';
    protected $allowedFields = ['username','password','name','email','created_at'];

    protected $beforeInsert = ['beforeInsert'];
    protected $beforeUpdate = ['beforeUpdate'];

    protected function beforeInsert(array $data){
        $data = $this->passwordHash($data);
        return $data;
    }
    protected function beforeUpdate(array $data){
        $data = $this->passwordHash($data);
        return $data;
    }
    protected function passwordHash(array $data){
        if(isset($data['data']['password']))
        $data['data']['password'] = password_hash($data['data']['password'], PASSWORD_DEFAULT);

        return $data;
    }
    public function getScc(){
        
        return $this;
        exit;
        return $this->find();
        
        try{
            echo "1234";
        $i = 1;
        
        $query = $db->findAll();
        
        }
        catch (\Exception $e)
        {
            die($e->getMessage());
        }
        
       
        return $this->findAll();

    }
    
}