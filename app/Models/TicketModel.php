<?php namespace App\Models;

use CodeIgniter\Model;

class TicketModel extends Model{

    
    protected $table = 'support_tickets';
    // protected $primaryKey = 'id';
    protected $allowedFields = ['ticket_id','firebase_uuid','title','status'];

    // protected $beforeInsert = ['beforeInsert'];
    // protected $beforeUpdate = ['beforeUpdate'];

    public function allTickets(){
        return $this->orderBy('created_at', 'DESC')->findAll();
    }
    public function openTickets(){
        return $this->where('status', 'open')->orderBy('created_at', 'DESC')->findAll();
    }
    public function ongoingTickets(){
        return $this->where('status', 'ongoing')->orderBy('created_at', 'DESC')->findAll();
    }
    public function closedTickets(){
        return $this->where('status', 'closed')->orderBy('created_at', 'DESC')->findAll();
    }
    public function checkStatus($tid){
        return $this->where('ticket_id', $tid)->find();
    }
    public function updateStatus($tid,$status){
        return $this->
            whereIn('ticket_id', $tid)->
            set(['status' => $status])->
            update();
    }
    public function latestTickets(){
        return $this->
            whereIn('status', 'open')->
            orderBy('created_at', 'DESC')->
            limit(5)->
            findAll();
    }

}