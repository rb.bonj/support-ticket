<?php namespace App\Models;

use CodeIgniter\Model;

class ConvoModel extends Model{

    
    protected $table = 'support_tickets_convo';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'ticket_id',
        'description',
        'staff_id',
        'attach_s3',
        'attach_filename',
        'created_at'
    ];


    public function getticketConvo($ticket_id){
        return $this->
            join('support_tickets', 'support_tickets.ticket_id = support_tickets_convo.ticket_id', 'LEFT')->
            select('support_tickets.ticket_id')->
            select('support_tickets.status')->
            select('support_tickets_convo.description')->
            select('support_tickets_convo.created_at')->
            select('support_tickets_convo.staff_id')->
            where('support_tickets_convo.ticket_id',$ticket_id)->
            orderBy('support_tickets_convo.created_at', 'DESC')->
            find();
    }
    public function replyTicket($data){
        return $this->insert($data);
    }
}