<?php namespace App\Controllers;

use App\Models\AdminModel;
use CodeIgniter\RESTful\ResourceController;

class Admin extends BaseController
{
	public function index()
	{
		return view('index');
    }
    public function login()
    {
        if(isset($_POST['btnLogin'])){
            if($this->exists($this->request->getVar('useremail'),$this->request->getVar('password')) != NULL){
                $session = session();
                $session->set('email',$this->request->getVar('useremail'));
                return $this->response->redirect(site_url('index'));
            }else{
                $data['msg'] = 'Email or Password is Incorrect.';
                return view('admin/login',$data);
            }
        }else{
            return view('admin/login');
        }
    }
    private function exists($email, $password)
    {
        
        $adminmodel = new AdminModel();
        $a_m = $adminmodel->where(array('email' =>$email, 'password' => $password))->first();
        if($a_m != NULL){
            return $a_m;
            //checking if password in hash
            // if(password_verify($password, $a_m['password'])){ }
        }
    }
    public function dashboard(){
        $session = session();
        if($session->email != NULL){
            return view('admin/dashboard');
        }else{
            return $this->response->redirect(site_url('../'));
        }
    }
    public function signout(){
        $session = session();
        $session->remove('email');
        $session->destroy();
        return $this->response->redirect(site_url('admin/login'));
    }
    private function register()
	{
		helper('form');
		$data = [];

		if ($this->request->getMethod() != 'post')
			return $this->fail('Only post request is allowed');


		$rules = [
            'username' => ['rules' => 'required|min_length[6]', 'label' => 'Username'],
            'password' => 'required|min_length[8]',
			'password_confirm' => 'matches[password]',
			'name' => 'required|max_length[30]',
			'email' => 'required|valid_email|is_unique[users.email]',
		];

		if (!$this->validate($rules)) {
			return $this->fail(implode('<br>', $this->validator->getErrors()));
		} else {
			$model = new AdminModel();

			$data = [
				'username' => $this->request->getVar('username'),
				'password' => $this->request->getVar('password'),
				'name' => $this->request->getVar('name'),
				'email' => $this->request->getVar('email'),
			];

			$user_id = $model->insert($data);
			$data['id'] = $user_id;
			unset($data['password']);

			return $this->respondCreated($data);
		}
    }
    public function create()
	{
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            return $this->respond(array('status' => '403', 'message' => 'Invalid Request', 'data' => []));
        }else{
            $rules = [
                'username' => ['rules' => 'required|min_length[6]', 'label' => 'Username'],
                'password' => 'required|min_length[8]',
                'password_confirm' => 'matches[password]',
                'name' => 'required|max_length[30]',
                'email' => 'required|valid_email|is_unique[support_staff.email]',
                'create_at' => 'required'
            ];
            
            if (!$this->validate($rules)) {
                return $this->fail(implode('<br>', $this->validator->getErrors()));
            } else {
                $data = [
                    'username' => $this->request->getVar('username'),
                    'password' => $this->request->getVar('password'),
                    'name' => $this->request->getVar('name'),
                    'email' => $this->request->getVar('email'),
                    'create_at' => $this->request->getVar('created_at')
                ];
               
                $i = $this->adminmodel->insert($data);
                $data['id'] = $i;
                return $this->respondCreated($data);
            }
        }
		
    }
}
