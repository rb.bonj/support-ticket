<?php namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;

class Api extends ResourceController
{
    protected $modelName = 'App\Models\AdminModel';
	protected $format = 'json';

    public function registersupport()
	{
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            return $this->respond(array('status' => '403', 'message' => 'Invalid Request', 'data' => []));
        }else{
            $rules = [
                'username' => ['rules' => 'required|min_length[6]|is_unique[support_staff.username]', 'label' => 'Username'],
                'password' => 'required|min_length[8]',
                'password_confirm' => 'matches[password]',
                'name' => 'required|max_length[30]',
                'email' => 'required|valid_email|is_unique[support_staff.email]',
                'created_at' => 'required'
            ];
            
            if (!$this->validate($rules)) {
                return $this->fail(implode('<br>', $this->validator->getErrors()));
            } else {
                $data = [
                    'username' => $this->request->getVar('username'),
                    'password' => $this->request->getVar('password'),
                    'name' => $this->request->getVar('name'),
                    'email' => $this->request->getVar('email'),
                    'created_at' => $this->request->getVar('created_at')
                ];
               
                $i = $this->model->insert($data);
                $data['id'] = $i;
                return $this->respondCreated($data);
            }
        }
    }
}

