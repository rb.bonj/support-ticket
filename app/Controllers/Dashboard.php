<?php namespace App\Controllers;

use App\Models\AdminModel;
use App\Models\TicketModel;
use App\Models\ConvoModel;
use CodeIgniter\RESTful\ResourceController;

class Dashboard extends BaseController
{
    public function index(){
        $session = session();
        if($session->email != NULL){
            $model = new TicketModel();
            $data['latestticket'] = $model->latestTickets();
            $data['alltickets'] = $model->allTickets();
            return view('admin/main',$data);
        }else{
            return $this->response->redirect(site_url('../'));
        }
    }
	public function open(){
        $session = session();
        if($session->email != NULL){
            $model = new TicketModel();
            $data['latestticket'] = $model->latestTickets();
            $data['opentickets'] = $model->openTickets();
            $getdata = $data['opentickets'];
            if($getdata){
                $data['opentickets'] = $getdata;
            }else{
                $data['nodata'] = "nodata"; 
            }
            return view('admin/open',$data);
        }else{
            return $this->response->redirect(site_url('../'));
        }
    }
    public function ongoing(){
        $session = session();
        if($session->email != NULL){
            $model = new TicketModel();
            $data['latestticket'] = $model->latestTickets();
            $data['ongoingtickets'] = $model->ongoingTickets();
            $getdata = $data['ongoingtickets'];
            if($getdata){
                $data['ongoingtickets'] = $getdata;
            }else{
                $data['nodata'] = "nodata";
            }
            return view('admin/ongoing',$data);
        }else{
            return $this->response->redirect(site_url('../'));
        }
    }
    public function closed(){
        $session = session();
        if($session->email != NULL){
            $model = new TicketModel();
            $data['latestticket'] = $model->latestTickets();
            $data['closedtickets'] = $model->closedTickets();
            $getdata = $data['closedtickets'];
            if($getdata){
                $data['closedtickets'] = $getdata;
            }else{
                $data['nodata'] = "nodata";
            }
            return view('admin/closed',$data);
        }else{
            return $this->response->redirect(site_url('../'));
        }
    }
    public function conversation($ticket_id){
        $session = session();
        if($session->email != NULL){
            $m = new ConvoModel();
            $model = new TicketModel();
            $data['latestticket'] = $model->latestTickets();
            $data['convo'] = $m->getticketConvo($ticket_id);
            $status = $data['convo'];
            foreach($status as $s){
                $data['status'] = $s['status'];
            }
            return view('admin/conversation',$data);
        }else{
            return $this->response->redirect(site_url('../'));
        }
    }
    public function ticket_convo(){
        $session = session();
        if($session->email != NULL){
            $m = new ConvoModel();
            $st = new TicketModel();
            $tid = $this->request->getVar('ticket_id');
            if(isset($_POST['reply-ticket'])){
                
                $data = [
                    'ticket_id' => $this->request->getVar('ticket_id'),
                    'description' => $this->request->getVar('message'),
                    'staff_id' => 1,
                    'attach_s3' => 2,
                    'attach_filename' => 3
                ];
                $checkstatus = $st->checkstatus($tid);
                if($checkstatus){
                    foreach($checkstatus as $cs){
                        if($cs['status'] != 'open'){
                            $insertreply = $m->replyTicket($data);
                        }else{
                            $status = "ongoing";
                            $updatestatus = $st->updateStatus($tid,$status);
                            if($updatestatus){
                                $insertreply = $m->replyTicket($data);
                            }
                        }
                    }
                }
            }else if(isset($_POST['close-ticket'])){
                $status = "closed";
                $updatestatus = $st->updateStatus($tid,$status);
            }
            return $this->response->redirect(site_url('conversation/'.$tid));
            
        }else{
            return $this->response->redirect(site_url('../'));
        }
    }
    // public function sidenotif(){

    //         $m = new ConvoModel();
    //         $data['convo'] = $m->getticketConvo($ticket_id);
    //         $status = $data['convo'];
    //         foreach($status as $s){
    //             $data['status'] = $s['status'];
    //         }
    //         return view('admin/conversation',$data);
    // }
}
