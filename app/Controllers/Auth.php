<?php namespace App\Controllers;

use App\Models\AuthModel;

class Auth extends BaseController
{

    public function account()
	{
        // validation
        $val = $this->validate([
            'email' => 'required'
        ]);
        if (!$val) {
            return view('account', ['validation' => $this->validator->getErrors()]);
        } else {
            if(isset($_POST['uregister'])){
                $data = [
                    'email' => $this->request->getVar('email')
                ];
                //$i = $this->model->insert($data);
                // $data['id'] = $i;
                // return $this->respondCreated($data);
            }else{
                return view('account');
            }
            
        }
        // $validation =  \Config\Services::validation();
        // $validation->setRule('email', 'Email', 'required');
        // if (! $this->validate([]))
        // {
        //     echo $this->request->getVar('email');
        //     echo view('account', ['validation' => $this->validator]);
        // }
        // else
        // {
        //     echo "asd";
        // }
	}
    public function login()
    {
        echo "login";
    }
    public function register(){
        $model = new AuthModel();
        if(!$this->validate([
            'email' => 'required',
            'password' => 'required|min_length[8]|max_length[24]'
        ])){
            return view('account');
        }else{
            $model->save([
                'email' => $this->request->getVar('email'),
                'password' => $this->request->getVar('password')
            ]);
            return redirect()->to('/');
        }
    }
    
}
