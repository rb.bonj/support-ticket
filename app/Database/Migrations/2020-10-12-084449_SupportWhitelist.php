<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class SupportWhitelist extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'ip_address'	=> [
				'type'          => 'VARCHAR',
				'constraint'    => 100,
				'unique'		=> true,
			],
			'created_at'	=> [
				'type'          => 'DATETIME',
			]
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('support_whitelist');
	}

	public function down()
	{
		$this->forge->dropTable('support_whitelist');
	}
}
