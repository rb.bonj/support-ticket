<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class SupportStaff extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id'	=> [
				'type'          => 'INT',
				'constraint'    => 11,
				'auto_increment'=> true,
			],
			'username'	=> [
				'type'          => 'VARCHAR',
				'constraint'    => '100',
			],
			'password'	=> [
				'type'          => 'VARCHAR',
				'constraint'	=> '100',
			],
			'name'	=> [
				'type'			=> 'VARCHAR',
				'constraint'	=> '150',
			],
			'email'	=> [
				'type'			=> 'VARCHAR',
				'constraint'	=> '100',
			],
			'created_at' => [
				'type'			=> 'DATETIME'
			]
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('support_staff');
	}

	public function down()
	{
		$this->forge->dropTable('support_staff');
	}
}
