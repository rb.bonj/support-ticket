<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class SupportTickets extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id'	=> [
				'type'          => 'INT',
				'constraint'    => 11,
				'auto_increment'=> true,
			],
			'ticket_id'	=> [
				'type'          => 'VARCHAR',
				'constraint'    => '150',
			],
			'firebase_uid'	=> [
				'type'          => 'VARCHAR',
				'constraint'	=> '100',
			],
			'title'	=> [
				'type'			=> 'VARCHAR',
				'constraint'	=> '150',
			],
			'email'	=> [
				'type'			=> 'VARCHAR',
				'constraint'	=> '100',
			],
			'description' => [
				'type'			=> 'TEXT'
			],
			'status'	=> [
				'type'          => 'VARCHAR',
				'constraint'    => '20',
			],
			'staff_id'	=> [
				'type'          => 'int',
			],
			'attach_s3'	=> [
				'type'			=> 'VARCHAR',
				'constraint'	=> '150',
			],
			'attach_filename'	=> [
				'type'			=> 'VARCHAR',
				'constraint'	=> '150',
			],
			'created_at' => [
				'type'			=> 'DATETIME'
			]
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('support_tickets');
	}

	public function down()
	{
		$this->forge->dropTable('support_tickets');
	}
}
