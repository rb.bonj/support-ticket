<div class="product-item-filter row">
                            <div class="col-12 col-sm-6 text-center text-sm-left">
                                <div class="search-product">
                                    <form action="#">
                                        <input class="form-control" placeholder="Search here..." type="text">
                                        <button type="submit"> <i class="fa fa-search"></i> </button>
                                    </form>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 text-center text-sm-left">
                                <div class="toolbar-sorter-right">
                                    <span>Sort by </span>
                                    <select id="basic" class="selectpicker show-tick form-control" data-placeholder="$ USD">
									<option data-display="Select">Nothing</option>
									<option value="1">Popularity</option>
									<option value="2">High Price → High Price</option>
									<option value="3">Low Price → High Price</option>
									<option value="4">Best Selling</option>
								</select>
                                </div>
                                <p>Showing all 4 results</p>
                            </div>
                        </div>