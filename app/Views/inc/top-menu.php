<div class="main-top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="text-slid-box">
                        <div id="offer-box" class="carouselTicker">
                            <ul class="offer-box">
								<li>
                                    <i class="fab fa-opencart"></i> Latest Gold Price (Per Gram)
                                </li>
                                <li>
                                    <i class="fab fa-opencart"></i> 999.9 Gold - SGD$82.15
                                </li>
                                <li>
                                    <i class="fab fa-opencart"></i> 916 Gold - SGD$69.31
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="custom-select-box">
                        <select id="basic" class="selectpicker show-tick form-control" data-placeholder="$ USD">
						<option>$ USD</option>
						<option>¥ JPY</option>
						<option>€ EUR</option>
					</select>
                    </div>
                    <div class="right-phone-box">
                        <p>Call US :- <a href="#"> +65 6372 5050    </a></p>
                    </div>
                    <div class="our-link">
                        <ul>
                            <li><a href="<?=$baseUrl.'/account'?>">My Account</a></li>
                            <li><a href="#">Our location</a></li>
                            <li><a href="#">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>