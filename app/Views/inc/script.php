<script src="<?=BASE_URL()?>/assets/js/jquery-3.2.1.min.js"></script>
<script src="<?=BASE_URL()?>/assets/js/popper.min.js"></script>
<script src="<?=BASE_URL()?>/assets/js/bootstrap.min.js"></script>
<!-- ALL PLUGINS -->
<script src="<?=BASE_URL()?>/assets/js/jquery.superslides.min.js"></script>
<script src="<?=BASE_URL()?>/assets/js/bootstrap-select.js"></script>
<script src="<?=BASE_URL()?>/assets/js/inewsticker.js"></script>
<script src="<?=BASE_URL()?>/assets/js/bootsnav.js"></script>
<script src="<?=BASE_URL()?>/assets/js/images-loded.min.js"></script>
<script src="<?=BASE_URL()?>/assets/js/isotope.min.js"></script>
<script src="<?=BASE_URL()?>/assets/js/owl.carousel.min.js"></script>
<script src="<?=BASE_URL()?>/assets/js/baguetteBox.min.js"></script>
<script src="<?=BASE_URL()?>/assets/js/form-validator.min.js"></script>
<script src="<?=BASE_URL()?>/assets/js/contact-form-script.js"></script>
<script src="<?=BASE_URL()?>/assets/js/custom.js"></script>