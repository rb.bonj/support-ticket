<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Dashboard">
        <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

        <title>Dashboard | Support-Ticket</title>

        <!-- Bootstrap core CSS -->
        <link href="<?=$baseUrl?>/assets/css/bootstrap.css" rel="stylesheet">
        <!--external css-->
        <link href="<?=$baseUrl?>/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="<?=$baseUrl?>/assets/css/zabuto_calendar.css">
        <link rel="stylesheet" type="text/css" href="<?=$baseUrl?>/assets/js/gritter/css/jquery.gritter.css" />
        <link rel="stylesheet" type="text/css" href="<?=$baseUrl?>/assets/lineicons/style.css">    
    
        <!-- Custom styles for this template -->
        <link href="<?=$baseUrl?>/assets/css/style.css" rel="stylesheet">
        <link href="<?=$baseUrl?>/assets/css/style-responsive.css" rel="stylesheet">

        <script src="<?=$baseUrl?>/assets/js/chart-master/Chart.js"></script>
        <style>

            .cons {
            border: 2px solid #dedede;
            background-color: #f1f1f1;
            border-radius: 5px;
            padding: 10px;
            margin: 10px 0;
            }

            .darker {
            border-color: #ccc;
            background-color: #ddd;
            }

            .cons::after {
            content: "";
            clear: both;
            display: table;
            }

            .cons img {
            float: left;
            max-width: 60px;
            width: 100%;
            margin-right: 20px;
            border-radius: 50%;
            }

            .cons img.right {
            float: right;
            margin-left: 20px;
            margin-right:0;
            }

            .time-right {
            float: right;
            color: #aaa;
            }

            .time-left {
            float: left;
            color: #999;
            }
        </style>
    </head>

    <body>
    <section id="container" >
        <?php include 'inc/nav.php'?>
        <?php include 'inc/aside.php'?>
        <section id="main-content">
            <section class="wrapper">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="row mt">
                            <div class="col-md-12">
                                <div class="">
                                    <h2>&nbsp;Conversation</h2>
                                    <form action="<?=$baseUrl.'/Dashboard/ticket_convo'?>" method="POST">
                                        <?php if($status != 'closed'){?>
                                            <div class="col-md-9">
                                                <textarea name="message" id="" cols="100" rows="5" style="resize: none;" class="form-control"></textarea>
                                            </div>
                                            <div class="col-md-3" style="margin-top: -2%;">
                                                <br>
                                                <input type="file" name="" id=""><br>
                                                <input type="submit" name="reply-ticket" class="btn btn-primary" style="width: 200px; margin-bottom: 5%;" value="Reply Ticket">
                                                <input type="submit" name="close-ticket" class="btn btn-default" style="width: 200px;" value="Close Ticket">
                                            </div>
                                        <?php }?>
                                        <div class="col-md-12">
                                            <?php foreach($convo as $d):?>
                                            <input hidden type="text" name="ticket_id" value="<?=$d['ticket_id']?>">
                                                <?php if($d['staff_id'] == ''){?>
                                                    <div class="cons">
                                                        <img src="<?=$baseUrl?>/assets/img/fr-05.jpg" alt="Avatar" style="width:100%;">
                                                        <p><?=$d['description']?></p>
                                                        <span class="time-right"><?=$d['created_at']?></span>
                                                    </div>
                                                <?php }else{?>
                                                    <div class="cons darker">
                                                        <img src="<?=$baseUrl?>/assets/img/fr-05.jpg" alt="Avatar" class="right" style="width:100%;">
                                                        <p><?=$d['description']?></p>
                                                        <span class="time-left"><?=$d['created_at']?></span>
                                                    </div>
                                                <?php }?>
                                            <?php endforeach;?>
                                        </div>
                                    </form>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                <?php include 'inc/right.php'?>
            </section>
        </section>
        <footer class="site-footer">
            <div class="text-center">
                2014 - Alvarez.is
                <a href="index.html#" class="go-top">
                    <i class="fa fa-angle-up"></i>
                </a>
            </div>
        </footer>
      <!--footer end-->
    </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?=$baseUrl?>/assets/js/jquery.js"></script>
    <script src="<?=$baseUrl?>/assets/js/jquery-1.8.3.min.js"></script>
    <script src="<?=$baseUrl?>/assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="<?=$baseUrl?>/assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?=$baseUrl?>/assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?=$baseUrl?>/assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="<?=$baseUrl?>/assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="<?=$baseUrl?>/assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="<?=$baseUrl?>/assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="<?=$baseUrl?>/assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="<?=$baseUrl?>/assets/js/sparkline-chart.js"></script>    
	<script src="<?=$baseUrl?>/assets/js/zabuto_calendar.js"></script>	
	
	<!-- <script type="text/javascript">
        $(document).ready(function () {
        var unique_id = $.gritter.add({
            title: 'Welcome to Dashgum!',
            text: 'Hover me to enable the Close Button. You can hide the left sidebar clicking on the button next to the logo. Free version for <a href="http://blacktie.co" target="_blank" style="color:#ffd777">BlackTie.co</a>.',
            image: 'assets/img/ui-sam.jpg',
            sticky: true,
            time: '',
            class_name: 'my-sticky-class'
        });

        return false;
        });
	</script> -->
  </body>
</html>
