<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Dashboard">
        <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

        <title>Dashboard | Support-Ticket</title>

        <!-- Bootstrap core CSS -->
        <link href="<?=$baseUrl?>/assets/css/bootstrap.css" rel="stylesheet">
        <!--external css-->
        <link href="<?=$baseUrl?>/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="<?=$baseUrl?>/assets/css/zabuto_calendar.css">
        <link rel="stylesheet" type="text/css" href="<?=$baseUrl?>/assets/js/gritter/css/jquery.gritter.css" />
        <link rel="stylesheet" type="text/css" href="<?=$baseUrl?>/assets/lineicons/style.css">    
    
        <!-- Custom styles for this template -->
        <link href="<?=$baseUrl?>/assets/css/style.css" rel="stylesheet">
        <link href="<?=$baseUrl?>/assets/css/style-responsive.css" rel="stylesheet">

        <script src="<?=$baseUrl?>/assets/js/chart-master/Chart.js"></script>
    </head>

    <body>
    <section id="container" >
        <?php include 'inc/nav.php'?>
        <?php include 'inc/aside.php'?>
        <section id="main-content">
            <section class="wrapper">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="row mt">
                            <div class="col-md-12">
                                <div class="content-panel">
                                    <table class="table table-striped table-advance table-hover">
	                  	  	            <h4><i class="fa fa-angle-right"></i> Closed Table</h4>
	                  	  	            <hr>
                                            <?php if($nodata){?>
                                                <h3 align="center">No Data Found</h3>
                                            <?php }else{?>
                                            <thead>
                                                <tr>
                                                    <th>Ticket ID</th>
                                                    <th>Title</th>
                                                    <th>Created at</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach($closedtickets as $r): ?>
                                                <tr>
                                                    <td><a href="<?=site_url('conversation/').$r['ticket_id']?>"><?=$r['ticket_id']?></a></td>
                                                    <td><?=$r['title']?></td>
                                                    <td><?=$r['created_at']?></td>
                                                    <td>
                                                        <button class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>
                                                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button>
                                                        <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button>
                                                    </td>
                                                </tr>
                                                <?php endforeach; ?>    
                                            </tbody>
                                            <?php }?>
                                        </hr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php include 'inc/right.php'?>
            </section>
        </section>
        <footer class="site-footer">
            <div class="text-center">
                2014 - Alvarez.is
                <a href="index.html#" class="go-top">
                    <i class="fa fa-angle-up"></i>
                </a>
            </div>
        </footer>
      <!--footer end-->
    </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?=$baseUrl?>/assets/js/jquery.js"></script>
    <script src="<?=$baseUrl?>/assets/js/jquery-1.8.3.min.js"></script>
    <script src="<?=$baseUrl?>/assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="<?=$baseUrl?>/assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?=$baseUrl?>/assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?=$baseUrl?>/assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="<?=$baseUrl?>/assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="<?=$baseUrl?>/assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="<?=$baseUrl?>/assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="<?=$baseUrl?>/assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="<?=$baseUrl?>/assets/js/sparkline-chart.js"></script>    
	<script src="<?=$baseUrl?>/assets/js/zabuto_calendar.js"></script>	
	
	<!-- <script type="text/javascript">
        $(document).ready(function () {
        var unique_id = $.gritter.add({
            title: 'Welcome to Dashgum!',
            text: 'Hover me to enable the Close Button. You can hide the left sidebar clicking on the button next to the logo. Free version for <a href="http://blacktie.co" target="_blank" style="color:#ffd777">BlackTie.co</a>.',
            image: 'assets/img/ui-sam.jpg',
            sticky: true,
            time: '',
            class_name: 'my-sticky-class'
        });

        return false;
        });
	</script> -->
  </body>
</html>
