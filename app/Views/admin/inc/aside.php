<aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <p class="centered"><a href="profile.html"><img src="<?=$baseUrl?>/assets/img/ui-sam.jpg" class="img-circle" width="60"></a></p>
              	  <h5 class="centered"><?=$_SESSION['email']?></h5>
              	  	
                    <li class="mt">
                      <a class="active" href="<?=site_url().'/index'?>">
                          <i class="fa fa-dashboard"></i>
                          <span>Support</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                  <a href="<?=site_url().'/open'?>">
                          <i class="fa fa-desktop"></i>
                          <span>OPEN</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                  <a href="<?=site_url().'/ongoing'?>" >
                          <i class="fa fa-cogs"></i>
                          <span>ONGOING</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                  <a href="<?=site_url().'/closed'?>" >
                          <i class="fa fa-book"></i>
                          <span>CLOSED</span>
                      </a>
                  </li>
                  

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>