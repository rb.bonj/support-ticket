    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Core CSS - Include with every page -->
    <link href="<?=$baseUrl?>/admin/assets/plugins/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="<?=$baseUrl?>/admin/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="<?=$baseUrl?>/admin/assets/plugins/pace/pace-theme-big-counter.css" rel="stylesheet" />
    <link href="<?=$baseUrl?>/admin/assets/css/style.css" rel="stylesheet" />
    <link href="<?=$baseUrl?>/admin/assets/css/main-style.css" rel="stylesheet" />