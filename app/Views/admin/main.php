<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Dashboard">
        <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

        <title>Dashboard | Support-Ticket</title>

        <!-- Bootstrap core CSS -->
        <link href="<?=$baseUrl?>/assets/css/bootstrap.css" rel="stylesheet">
        <!--external css-->
        <link href="<?=$baseUrl?>/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="<?=$baseUrl?>/assets/css/zabuto_calendar.css">
        <link rel="stylesheet" type="text/css" href="<?=$baseUrl?>/assets/js/gritter/css/jquery.gritter.css" />
        <link rel="stylesheet" type="text/css" href="<?=$baseUrl?>/assets/lineicons/style.css">    
    
        <!-- Custom styles for this template -->
        <link href="<?=$baseUrl?>/assets/css/style.css" rel="stylesheet">
        <link href="<?=$baseUrl?>/assets/css/style-responsive.css" rel="stylesheet">

        <script src="<?=$baseUrl?>/assets/js/chart-master/Chart.js"></script>
    </head>

    <body>
    <section id="container" >
        <?php include 'inc/nav.php'?>
        <?php include 'inc/aside.php'?>
        <section id="main-content">
            <section class="wrapper">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="row mt">
                            <div class="col-md-12">
                                <div class="content-panel">
                                    <section class="task-panel tasks-widget">
                                        <div class="panel-heading">
                                            <div class="pull-left"><h5><i class="fa fa-tasks"></i> All Ticket</h5></div>
                                            <br>
                                        </div>
                                        <div class="panel-body">
                                            <div class="task-content">
                                                <ul id="sortable" class="task-list ui-sortable">
                                                    <?php foreach($alltickets as $r):?>
                                                        <?php if($r['status'] == 'open'){?>
                                                            <li class="list-primary">
                                                        <?php }elseif($r['status'] == 'ongoing'){?>
                                                            <li class="list-warning">
                                                        <?php }elseif($r['status'] == 'closed'){?>
                                                            <li class="list-danger">
                                                        <?php }?>
                                                            <i class=" fa fa-ellipsis-v"></i>
                                                            <a href="<?=site_url('conversation/').$r['ticket_id']?>">
                                                                <p>Ticket ID: <?=$r['ticket_id']?></p>
                                                                <div class="task-title">
                                                                    <span class="task-title-sp"><?=$r['title']?></span>
                                                                    <?php if($r['status'] == 'open'){?>
                                                                        <span class="badge bg-theme">
                                                                    <?php }elseif($r['status'] == 'ongoing'){?>
                                                                        <span class="badge bg-warning">
                                                                    <?php }elseif($r['status'] == 'closed'){?>
                                                                        <span class="badge bg-important">
                                                                    <?php }?>
                                                                        <?=$r['status']?>
                                                                    </span>
                                                                    <div class="pull-right hidden-phone">
                                                                        <button class="btn btn-success btn-xs fa fa-check"></button>
                                                                        <button class="btn btn-primary btn-xs fa fa-pencil"></button>
                                                                        <button class="btn btn-danger btn-xs fa fa-trash-o"></button>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </li>
                                                    <?php endforeach;?>
                                                    <!-- <li class="list-primary">
                                                        <i class=" fa fa-ellipsis-v"></i>
                                                        <a href="<?//=site_url('conversation/').'123'?>">
                                                            <p>Ticket ID: </p>
                                                            <div class="task-title">
                                                                <span class="task-title-sp">Dashgum - Admin Panel Theme</span>
                                                                <span class="badge bg-theme">Open</span>
                                                                <div class="pull-right hidden-phone">
                                                                    <button class="btn btn-success btn-xs fa fa-check"></button>
                                                                    <button class="btn btn-primary btn-xs fa fa-pencil"></button>
                                                                    <button class="btn btn-danger btn-xs fa fa-trash-o"></button>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </li>
                                                    <li class="list-warning">
                                                        <i class=" fa fa-ellipsis-v"></i>
                                                        <a href="<?//=site_url('conversation/').'12345'?>">
                                                            <p>Ticket ID: </p>
                                                            <div class="task-title">
                                                                <span class="task-title-sp">Extensive collection of plugins</span>
                                                                <span class="badge bg-warning">Ongoing</span>
                                                                <div class="pull-right hidden-phone">
                                                                    <button class="btn btn-success btn-xs fa fa-check"></button>
                                                                    <button class="btn btn-primary btn-xs fa fa-pencil"></button>
                                                                    <button class="btn btn-danger btn-xs fa fa-trash-o"></button>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </li>
                                                    <li class="list-danger">
                                                        <i class=" fa fa-ellipsis-v"></i>
                                                        <a href="<?//=site_url('conversation/').'12345'?>">
                                                            <p>Ticket ID: </p>
                                                            <div class="task-title">
                                                                <span class="task-title-sp">Hey, seriously, you should buy this Dashboard</span>
                                                                <span class="badge bg-important">Closed</span>
                                                                <div class="pull-right hidden-phone">
                                                                    <button class="btn btn-success btn-xs fa fa-check"></button>
                                                                    <button class="btn btn-primary btn-xs fa fa-pencil"></button>
                                                                    <button class="btn btn-danger btn-xs fa fa-trash-o"></button>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </li> -->
                                                </ul>
                                            </div>
                                            <div class=" add-task-row">
                                                Pagination
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php include 'inc/right.php'?>
            </section>
        </section>
        <footer class="site-footer">
            <div class="text-center">
                2014 - Alvarez.is
                <a href="index.html#" class="go-top">
                    <i class="fa fa-angle-up"></i>
                </a>
            </div>
        </footer>
      <!--footer end-->
    </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?=$baseUrl?>/assets/js/jquery.js"></script>
    <script src="<?=$baseUrl?>/assets/js/jquery-1.8.3.min.js"></script>
    <script src="<?=$baseUrl?>/assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="<?=$baseUrl?>/assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?=$baseUrl?>/assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?=$baseUrl?>/assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="<?=$baseUrl?>/assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="<?=$baseUrl?>/assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="<?=$baseUrl?>/assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="<?=$baseUrl?>/assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="<?=$baseUrl?>/assets/js/sparkline-chart.js"></script>    
	<script src="<?=$baseUrl?>/assets/js/zabuto_calendar.js"></script>	
	
	<!-- <script type="text/javascript">
        $(document).ready(function () {
        var unique_id = $.gritter.add({
            title: 'Welcome to Dashgum!',
            text: 'Hover me to enable the Close Button. You can hide the left sidebar clicking on the button next to the logo. Free version for <a href="http://blacktie.co" target="_blank" style="color:#ffd777">BlackTie.co</a>.',
            image: 'assets/img/ui-sam.jpg',
            sticky: true,
            time: '',
            class_name: 'my-sticky-class'
        });

        return false;
        });
	</script> -->
  </body>
</html>
