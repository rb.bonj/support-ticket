<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Login Support-ticket">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard,Support-Ticket,Help">

    <title>Login | Support</title>

    <link href="<?=$baseUrl?>/assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?=$baseUrl?>/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="<?=$baseUrl?>/assets/css/style.css" rel="stylesheet">
    <link href="<?=$baseUrl?>/assets/css/style-responsive.css" rel="stylesheet">
  </head>

  <body>
	  <div id="login-page">
	  	<div class="container">
          
                <form class="form-login" role="form" method="POST" action="<?=$baseUrl.'/admin/login'?>">
               
            
		        <h2 class="form-login-heading">sign in</h2>
		        <div class="login-wrap">
		            <input type="email" name="useremail" class="form-control" placeholder="Email Address" >
		            <br>
		            <input type="password" name="password" class="form-control" placeholder="Password"><br>
                    <?php if($msg != null){?>
                    <div class="alert alert-danger" role="alert">
                        <?=isset($msg) ? $msg:''?>
                    </div>
                    <?php }?>
		            <label class="checkbox">
		                <span class="pull-right">
		                    <a data-toggle="modal" href="login.html#myModal"> Forgot Password?</a>
		
		                </span>
		            </label>
                    <input type="submit" name="btnLogin" class="btn btn-theme btn-block" value="Login">
		
		        </div>
		
		          <!-- Modal -->
		          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
		              <div class="modal-dialog">
		                  <div class="modal-content">
		                      <div class="modal-header">
		                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                          <h4 class="modal-title">Forgot Password ?</h4>
		                      </div>
		                      <div class="modal-body">
		                          <p>Enter your e-mail address below to reset your password.</p>
		                          <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">
		
		                      </div>
		                      <div class="modal-footer">
		                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
		                          <button class="btn btn-theme" type="button">Submit</button>
		                      </div>
		                  </div>
		              </div>
		          </div>
		          <!-- modal -->
		
		      </form>	  	
	  	
	  	</div>
	  </div>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?=$baseUrl?>/assets/js/jquery.js"></script>
    <script src="<?=$baseUrl?>/assets/js/bootstrap.min.js"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="<?=$baseUrl?>/assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("<?=$baseUrl?>/assets/img/login-bg.jpg", {speed: 500});
    </script>


  </body>
</html>